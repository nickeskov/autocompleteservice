name := "AutocompleteService"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies ++= Seq(
  "org.mongodb.scala" %% "mongo-scala-driver" % "2.6.0",
  "com.typesafe.akka" %% "akka-stream" % "2.5.23",
  "com.typesafe.akka" %% "akka-actor" % "2.5.23",
  "com.typesafe.akka" %% "akka-remote" % "2.5.23",
  "com.typesafe.akka" %% "akka-http"   % "10.1.9",
  // "org.scalaj" %% "scalaj-http" % "2.4.2",
  "org.json4s" %% "json4s-native" % "3.6.7",
  "com.pauldijou" %% "jwt-json4s-common" % "3.1.0",
  "com.typesafe.akka" %% "akka-http-testkit" % "10.1.9" % Test
)
