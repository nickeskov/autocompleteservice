package ResourceReaders

import java.util.Properties
import java.io._


object AppConfig {
  def apply(): AppConfig = new AppConfig()
}


class AppConfig {
  val config = new Properties()
  config.load(new FileInputStream("src/main/resources/appconfig.properties"))

  val appPort: String = config.getProperty("app.port")
  val dbConnectionString: String = config.getProperty("db.connectionString")
  val dbName: String = config.getProperty("db.name")
  val dbCollectionName: String = config.getProperty("db.collection.cache.name")
  val dadataApiToken: String = config.getProperty("dadata.api.token")
}