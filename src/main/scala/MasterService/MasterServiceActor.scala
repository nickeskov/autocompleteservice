package MasterService

import ResourceReaders.AppConfig
import ServiceMessages.UsersRequest
import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import org.mongodb.scala._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Projections._
import org.mongodb.scala.model.ReplaceOptions

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util._

object MasterServiceActor {
  def props(appConfig: AppConfig): Props = Props(new MasterServiceActor(appConfig))
}

class MasterServiceActor(appConfig: AppConfig)
  extends Actor with ActorLogging {

  implicit val system: ActorSystem = ActorSystem("master-service-system")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  private val dbClient = MongoClient(appConfig.dbConnectionString)
  private val requestsCache = dbClient.getDatabase(appConfig.dbName).getCollection(appConfig.dbCollectionName)

  override def preStart(): Unit = log.info("Master actor started")
  override def postStop(): Unit =  {
    log.info("Master actor stopped")
    dbClient.close()
  }

  override def receive: Receive = {
    case UsersRequest(request) =>
      val sndr = sender()
      requestsCache.find(equal("_id", request)).projection(excludeId()).first().headOption().map(
        result => {
          if (result.isEmpty) daDataRequest(request).map(response => {
            log.info(s"DaData returned answer for request=$request")
            Unmarshal(response.entity).to[String].map(data => {
              saveToDb(request, data)
              sndr ! data
            })
          }) else {
            sndr ! result.get.getString("data")
            log.info(s"Record with _id=$request returned from DB")
          }
        }
      )
    case something =>
      val sndr = sender()
      log.warning(s"Master actor received: $something")
      sndr ! None
  }

  def daDataRequest(request: String): Future[HttpResponse] =
    Http().singleRequest(
      HttpRequest(
        method = HttpMethods.POST,
        uri = s"https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address",
        headers = List(
          headers.Authorization(
            headers.OAuth2BearerToken(appConfig.dadataApiToken)
          )
        ),
        entity = HttpEntity(
          ContentTypes.`application/json`,
          request
        )
      )
    )

  def saveToDb(id: String, data: String): Unit =
    requestsCache.replaceOne(
      equal("_id", id),
      Document("_id" -> id, "data" -> data),
      ReplaceOptions().upsert(true)
    ).toFuture().onComplete {
      case Success(_) => log.info(s"Inserted answer for query=$id")
      case Failure(_) =>  log.error(s"Insert error answer for query=$id")
    }
}