package WebServer

import MasterService.MasterServiceActor
import MasterService.ServiceMessages._
import ResourceReaders.AppConfig
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import akka.pattern._
import akka.util.Timeout

import scala.concurrent.duration._
import scala.concurrent.ExecutionContextExecutor
import scala.io.StdIn


object WebServer {
  def run(appConfig: AppConfig): Unit = {
    implicit val system: ActorSystem = ActorSystem("my-system")
    implicit val materializer: ActorMaterializer = ActorMaterializer()
    // needed for the future flatMap/onComplete in the end
    implicit val executionContext: ExecutionContextExecutor = system.dispatcher
    implicit val timeout: Timeout = Timeout(5.seconds)

    val serviceActor = system.actorOf(MasterServiceActor.props(appConfig))

    val route =
      path("get") {
        get {
          parameter("request") {
            request: String => {
              val data = serviceActor ? UsersRequest(request)
              complete(data.map(_.asInstanceOf[String]))
            }
          }
        } ~
        post {
          entity(as[String]) {
            request => {
              val data = serviceActor ? UsersRequest(request)
              complete(data.map(_.asInstanceOf[String]))
            }
          }
        }
      }

    val bindingFuture = Http().bindAndHandle(route, "localhost", appConfig.appPort.toInt)

    println(s"Server online at http://localhost:${appConfig.appPort}/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
  }

  def main(args: Array[String]) {
    run(AppConfig())
  }
}